package com.orangehr.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.orangehr.data.Users;

public class AddSystemUsersPage extends AbstractPage {
    public AddSystemUsersPage(WebDriver driver) {
        super(driver);
    }

    public AddSystemUsersPage addRandomUser() {
        helper.clearAndSetText(By.id("systemUser_employeeName_empName"), Users.ZIPFILE.getEmployeeName());
        helper.clearAndSetText(By.id("systemUser_userName"), Users.ZIPFILE.getName());
        helper.clearAndSetText(By.id("systemUser_password"), Users.ZIPFILE.getPassword());
        helper.clearAndSetText(By.id("systemUser_confirmPassword"), Users.ZIPFILE.getPassword());
        helper.untilElementVisible(By.id("btnSave")).click();
        return this;
    }

}