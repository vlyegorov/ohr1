package com.orangehr.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DashboardPage extends AbstractPage {
    public DashboardPage(WebDriver driver) {
        super(driver);
    }
    
    public RecordsPage getEmployeeRecords() {
    	helper.hover(driver, By.id("menu_time_viewTimeModule"));
        helper.hover(driver, By.id("menu_attendance_Attendance"));
        helper.hover(driver, By.id("menu_attendance_viewAttendanceRecord"));
        helper.untilElementVisible(By.id("menu_attendance_viewAttendanceRecord")).click();
        return new RecordsPage(driver);
    }
    
    public ViewSystemUsersPage getToUsers() {
    	helper.hover(driver, By.id("menu_admin_viewAdminModule"));
        helper.hover(driver, By.id("menu_admin_UserManagement"));
        helper.hover(driver, By.id("menu_admin_viewSystemUsers"));
        helper.untilElementVisible(By.id("menu_admin_viewSystemUsers")).click();
        return new ViewSystemUsersPage(driver);
    }
    
    public void logout() {
        helper.untilElementVisible(By.id("welcome")).click();
        helper.untilElementVisible(By.xpath("//*[@id=\"welcome-menu\"]/ul/li[2]/a")).click();
    }
}