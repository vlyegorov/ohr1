package com.orangehr.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DatePicker extends AbstractPage {
    public DatePicker(WebDriver driver) {
        super(driver);
    }
    
	public DatePicker clickOnDatePicker() {
		helper.untilElementVisible(By.cssSelector(".ui-datepicker-trigger")).click();
		return new DatePicker(driver); 
	}
	
	public DatePicker chooseMonth() {
		helper.untilElementVisible(By.cssSelector(".ui-datepicker-month")).click();
		return new DatePicker(driver);
	}
	
	public DatePicker chooseYear() {
		helper.untilElementVisible(By.cssSelector(".ui-datepicker-year")).click();
		return new DatePicker(driver);
	}
	
	public DatePicker chooseDay() {
		helper.untilElementVisible(By.cssSelector(".ui-datepicker-current-day")).click();
		return new DatePicker(driver);
	}
}
