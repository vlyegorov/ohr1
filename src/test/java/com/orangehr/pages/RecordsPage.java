package com.orangehr.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RecordsPage extends AbstractPage{
    
	
	public RecordsPage(WebDriver driver) {
        super(driver);
    }

	public RecordsPage clickView() {
        helper.untilElementVisible(By.id("btView")).click();
        return new RecordsPage(driver);
    }
	
	public RecordsPage clickAddAttendanceRecords() {
        helper.untilElementVisible(By.id("btnPunchOut")).click();
        return new RecordsPage(driver);
    }
	
	public RecordsPage setName() {
		helper.clearAndSetText(By.id("attendance_employeeName_empName"), "John Smith");
		return new RecordsPage(driver);
	}
}
