package com.orangehr.pages;

import com.orangehr.tools.Helper;
import com.orangehr.tools.PropertiesContext;
import org.openqa.selenium.WebDriver;

public abstract class AbstractPage {

    protected WebDriver driver;
    protected Helper helper;
    private static final String URL = PropertiesContext.getInstance().getProperty("URL");
    protected static PropertiesContext context = PropertiesContext.getInstance();

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
        this.helper = Helper.getInstance(driver);
    }

    public void reload() {
        driver.navigate().refresh();
    }

    public AbstractPage navigate() {
        driver.get(URL);
        return this;
    }

    public WebDriver getDriver() {
        return driver;
    }
}
