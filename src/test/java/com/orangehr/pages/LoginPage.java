package com.orangehr.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends AbstractPage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage getToLoginPage() {
        navigate();
        return this;
    }

    public DashboardPage authenticate(String name, String password) {
        helper.clearAndSetText(By.name("txtUsername"), name);
        helper.clearAndSetText(By.name("txtPassword"), password);
        helper.untilElementVisible(By.name("Submit")).click();
        return new DashboardPage(driver);
    }
}