package com.orangehr.pages;

import com.orangehr.tools.Helper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AccountPage extends AbstractPage {
    @FindBy(id = "menu_admin_viewAdminModule") 
    public WebElement menuAdmin;
    @FindBy(id = "menu_admin_UserManagement")
    public WebElement menuUserManagement;
    @FindBy(id = "menu_admin_viewSystemUsers")
    public WebElement menuUsers;
    Helper helper;
    @FindBy(css = "#welcome")
    private WebElement usernameOnHeader;

    public AccountPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    public String getCurrentUrl() {
        return this.driver.getCurrentUrl();
    }

    public String readUserName() {
        return usernameOnHeader.getText();
    }

    public AccountPage click(WebElement element) {
        element.click();
        return this;
    }
}
