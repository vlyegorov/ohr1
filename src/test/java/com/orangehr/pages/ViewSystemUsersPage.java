package com.orangehr.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ViewSystemUsersPage extends AbstractPage {
    public ViewSystemUsersPage(WebDriver driver) {
        super(driver);
    }

    public AddSystemUsersPage clickAddUser() {
        helper.untilElementVisible(By.id("btnAdd")).click();
        return new AddSystemUsersPage(driver);
    }
}
