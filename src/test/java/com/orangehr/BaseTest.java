package com.orangehr;

import com.orangehr.tools.Browser;
import com.orangehr.tools.Helper;
import com.orangehr.tools.PropertiesContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.asserts.SoftAssert;

import java.util.logging.Level;
import java.util.logging.Logger;

public class BaseTest {


    public SoftAssert softAssert;
    private PropertiesContext context;
    public WebDriver driver;
    private Helper helper;

    public WebDriver getDriver() {
        return driver;
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(final ITestContext testContext) {
        Logger logger = Logger.getLogger("org.openqa.selenium");
        logger.setLevel(Level.SEVERE);
        context = PropertiesContext.getInstance();
        try {
            driver = new Browser().startBrowser(); //Init webdriver
            helper = Helper.getInstance(driver); //Init helper
        } catch (WebDriverException e) {
            Reporter.log("Failed to initialize test", 1, true);
            Reporter.log(e.getMessage(), 1, true);
        }
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        if (driver != null) {
            try {
                driver.quit();
            } catch (Exception e) {
                Reporter.log("Failed to quit webdriver", 1, true);
            }
        }
    }
}


