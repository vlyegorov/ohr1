package com.orangehr.data;

public enum Users {
    ADMIN("Admin", "Admin", "admin"),
    ZIPFILE("John Smith", "ZipFile", "ZipFile");

	private String employeeName;
    private String name;
    private String password;

    Users(String employeeName, String name, String password) {
        this.employeeName = employeeName;
    	this.name = name;
        this.password = password;

    }

    public String getEmployeeName() {
    	return employeeName;
    }
    
    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

}