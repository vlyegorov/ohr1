package com.orangehr;

import com.orangehr.data.Users;
import com.orangehr.pages.AccountPage;
import com.orangehr.pages.DashboardPage;
import com.orangehr.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {

    @Test
    public void testLogin() {
        LoginPage loginPage = new LoginPage(driver).getToLoginPage();
        AccountPage accountPage = new AccountPage(driver);
        DashboardPage dashboardPage = loginPage.authenticate(Users.ADMIN.getName(), Users.ADMIN.getPassword());
        String actualUrl = accountPage.getCurrentUrl();
        String expectedUrlDashBoardUrl = "http://opensource.demo.orangehrmlive.com/index.php/dashboard";
        Assert.assertEquals(expectedUrlDashBoardUrl, actualUrl, "Incorrent URL");

        String actualUserNameOnHeader = accountPage.readUserName();
        Assert.assertEquals(actualUserNameOnHeader, "Welcome Admin", "Username on header should be equal to expected.");
        dashboardPage.logout();
    }
}


