package com.orangehr.tools;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/*import org.testng.Reporter;
import org.testng.annotations.Test;*/
//import io.qameta.allure.Step;

public class Browser {

	public WebDriver open() {
		WebDriver driver = startBrowser();
		driver.manage().window().maximize();
		return driver;
	}

	public WebDriver startBrowser() {
		String browser = System.getProperty("browser");
		WebDriver driver = null;
		
		if (browser == null || "chrome".equals(browser.toLowerCase())) {
			driver = startChrome();
		} else if ("firefox".equals(browser.toLowerCase())) {
			driver = startFirefox();
		} else
			throw new RuntimeException("\nUnsupported driver for browser: " + browser + "\n");
		return driver;
	}

	private WebDriver startChrome() {
		ChromeDriverManager.getInstance().setup();
		return new ChromeDriver();
	}

	private WebDriver startFirefox() {
		FirefoxDriverManager.getInstance().setup();
		return new FirefoxDriver();
	}

}