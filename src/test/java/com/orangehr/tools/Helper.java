package com.orangehr.tools;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Helper extends WebDriverWait {

    private static Helper helper;
    private WebDriver driver;

    public Helper(WebDriver driver) {
        super(driver, 15);
        this.driver = driver;
    }

    public static Helper getInstance(WebDriver driver) {
        if (helper == null) {
            helper = new Helper(driver);
        }
        return helper;
    }

    public void hover(WebDriver driver, By by) {
        Actions actions = new Actions(driver);
        actions.moveToElement(untilElementVisible(by)).build().perform();
    }

    public WebElement untilElementVisible(By by) {
        return this.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void clearAndSetText(By by, String text) {
        untilElementVisible(by).clear();
        if (text != null) {
            untilElementVisible(by).sendKeys(text);
        }
    }
    
    public void verifyIsUserSaved(String text) {
    	driver.getPageSource().contains(text);
    	}
}