package com.orangehr;

import com.orangehr.data.Users;
import com.orangehr.pages.DashboardPage;
import com.orangehr.pages.LoginPage;
import org.testng.annotations.Test;

public class LoginWithAddUser extends BaseTest{

	@Test
	public void LoginAndAddUser() {
		LoginPage loginPage = new LoginPage(driver).getToLoginPage();
        DashboardPage dashboardPage = loginPage.authenticate(Users.ADMIN.getName(), Users.ADMIN.getPassword());
        dashboardPage.getToUsers().clickAddUser().addRandomUser();
        dashboardPage.logout();
        loginPage.authenticate(Users.ZIPFILE.getName(), Users.ZIPFILE.getPassword());
		}
}